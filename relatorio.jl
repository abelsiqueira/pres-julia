"""`relatorio_populacional_estado(dados, estado)`

Relatório sobre as cidades do estado passado nos dados.
Assume-se que `dados` esteja na ordem
`Cidades`, `População`, `Área`.

`estado` é o nome do estado, que será usado no relatório e
no nome do arquivo.
"""
function relatorio_populacional_estado(dados, estado)
    open("relatorio-$(lowercase(estado)).txt", "w") do f
        cidades, pop, area = dados[:,1], dados[:,2], dados[:,3]
        N = length(cidades)
        dens = pop ./ area
        mais_pop = indmax(pop)
        menos_pop = indmin(pop)
        maior = indmax(area)
        menor = indmin(area)
        mais_densa = indmax(dens)
        menos_densa = indmin(dens)
        
        write(f, "O $estado possui $N cidades\n")
        i = maior
        write(f, "A maior delas é $(cidades[i]) com $(area[i]) km²\n")
        i = menor
        write(f, "A menor delas é $(cidades[i]) com $(area[i]) km²\n")
        i = mais_pop
        write(f, "A mais populosa delas é $(cidades[i]) com $(pop[i]) habitantes\n")
        i = menos_pop
        write(f, "A menos populosa delas é $(cidades[i]) com $(pop[i]) habitantes\n")
        i = mais_densa
        write(f, "A mais densa delas é $(cidades[i]) com $(dens[i]) hab/km²\n")
        i = menos_densa
        write(f, "A menos densa delas é $(cidades[i]) com $(dens[i]) hab/km²\n")
    end
end