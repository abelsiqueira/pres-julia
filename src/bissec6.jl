function bissec (a::Float64; tol::Float64 = 1e-6, x::Float64 = 1,
    y::Float64 = a)
  a < 1 && error("Raiz de numero negativo nao existe nos reais")
  m = (x+y)/2
  while abs(m^2-a) > tol
    m^2 > a ? (y = m) : (x = m)
    m = (x+y)/2
  end
  return m
end
