function bissec (a; tol = 1e-6, x = 1, y = a)
  a < 1 && error("Raiz de numero negativo nao existe nos reais")
  m = (x+y)/2
  while abs(m^2-a) > tol
    m^2 > a ? (y = m) : (x = m)
    m = (x+y)/2
  end
  return m
end
