function raizq1 (a)
  a < 0 && return im*raizq1(-a)
  x, y = 1, (a+1)/2
  while abs(x-y) > 1e-4
    x, y = y, y/2+a/(2*y)
  end
  return y
end
