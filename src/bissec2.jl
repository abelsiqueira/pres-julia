function bissec (a)
  a < 1 && error("Raiz de numero negativo nao existe nos reais")
  x, y = 1, a
  m = (x+y)/2
  while abs(m^2-a) > 1e-4
    m^2 > a ? (y = m) : (x = m)
    m = (x+y)/2
  end
  return m
end
