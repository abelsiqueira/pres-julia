using PyPlot

plot([1,2,3,4,5],[3,2,5,1,4])
axis([0,6,0,6]); savefig("../img/ex1.png"); close()

plot([1,2,3,4,5],[3,2,5,1,4],".")
axis([0,6,0,6]); savefig("../img/ex2.png"); close()

x = linspace(0,1,100);
plot(x, x.^2)
xlabel("Coiso")
ylabel("Outro coiso");
savefig("../img/ex3.png"); close()

plot(x, x.^3-x, "r", x, 4*x.*(1-x), "b--")
savefig("../img/ex4.png"); close()

plot(cos(2*pi*x), sin(2*pi*x), "o")
title("Círculo")
axis([-1.5,1.5,-1.5,1.5]); savefig("../img/ex5.png"); close()

plot(x, x.^2, "r")
plot(x,1-x.^2, color="blue", linewidth=2.0, linestyle="--")
savefig("../img/ex6.png"); close()

plot(x, x.^2.*sin(4*pi*x))
plot(x, x.^2, "r--", x, -x.^2, "r--")
title(L"Plot of $f(x) = x^2\sin(x)$")
savefig("../img/ex7.png"); close()
