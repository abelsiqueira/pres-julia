function bissec (a)
  x = 1
  y = a
  m = (x+y)/2
  while abs(m^2-a) > 1e-4
    if m^2 > a
      y = m
    else
      x = m
    end
    m = (x+y)/2
  end
  return m
end
