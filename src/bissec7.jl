function bissec (a::Number; tol::Number = 1e-6, x::Number = 1,
    y::Number = a)
  a < 1 && error("Raiz de numero negativo nao existe nos reais")
  m = (x+y)/2
  while abs(m^2-a) > tol
    m^2 > a ? (y = m) : (x = m)
    m = (x+y)/2
  end
  return m
end
