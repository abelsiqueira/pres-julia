function bhaskara(a, b, c)
  D = b^2 - 4*a*c;
  D = sqrt(D);
  return (-b+D)/(2*a), (-b-D)/(2*a)
end
