while n != 1
  println("n = ", n)
  if n % 2 == 0
    n = n/2
  else
    n = 3*n+1
  end
end
