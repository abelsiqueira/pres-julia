function normF(A::Matrix)
  (m,n) = size(A);
  s = 0.0
  for i = 1:m
    for j = 1:n
      s += A[i,j]^2
    end
  end
  return sqrt(s)
end
