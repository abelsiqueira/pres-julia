      subroutine dotF (N, X, Y, D)

        integer N
        double precision X(N), Y(N)
        double precision D

        integer i

        D = 0.0D0
        do i = 1,N
          D = D + X(i)*Y(i)
        end do

      end subroutine dotF
