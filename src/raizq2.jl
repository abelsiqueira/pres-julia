function raizq2 (a, tol, x)
  if a < 0
    y, k = raizq2(-a, tol, x)
    return im*y, k
  end
  k = 1
  y = x/2+a/(2*x)
  while abs(x-y) > tol
    x, y = y, y/2+a/(2*y)
    k += 1
  end
  return y, k
end
