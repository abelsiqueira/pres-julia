function raizq4 (a; tol=1e-4, x=1)
  if a < 0
    y, k = raizq4(-a, tol, x)
    return im*y, k
  end
  k = 1
  y = x/2+a/(2*x)
  while abs(x-y) > tol
    x, y = y, y/2+a/(2*y)
    k += 1
  end
  return y, k
end
