for n in [1 10 100 200]
  println("log10($n) = ", log10(n))
end

for i = 1:10
  println("$i^2 = $(i^2)")
end
