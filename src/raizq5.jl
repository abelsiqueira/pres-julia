function raizq5 (a::Float64; tol::Float64=1e-4, x::Float64=1.0)
  if a < 0
    y, k = raizq5(-a, tol, x)
    return im*y, k
  end
  k = 1
  y = x/2+a/(2*x)
  while abs(x-y) > tol
    x, y = y, y/2+a/(2*y)
    k += 1
  end
  return y, k
end
