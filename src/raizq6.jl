function raizq6 (a::Number; tol::Number=1e-4, x::Number=1.0)
  if a < 0
    y, k = raizq6(-a, tol, x)
    return im*y, k
  end
  k = 1
  y = x/2+a/(2*x)
  while abs(x-y) > tol
    x, y = y, y/2+a/(2*y)
    k += 1
  end
  return y, k
end
