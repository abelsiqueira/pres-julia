n = 100
s = 0.0
for i = 1:100
  v = rand(n)
  w = rand(n)
  d = [0.0]
  ccall(("dotf_", "fcode/libtestF.so"), Void, (Ptr{Int32}, Ptr{Float64},
    Ptr{Float64}, Ptr{Float64}), Int32[n], v, w, d)
  s += abs(d[1]-dot(v,w))
end
println("Erro = $s")
