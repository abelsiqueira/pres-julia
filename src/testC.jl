n = 100
s = 0.0
for i = 1:100
  v = rand(n)
  w = rand(n)
  d = ccall(("dotC", "ccode/libtestC.so"), Cdouble,
    (Cint, Ptr{Cdouble}, Ptr{Cdouble}), n, v, w)
  s += abs(d-dot(v,w))
end
println("Erro = $s")
