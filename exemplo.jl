function aprox_log(x)
    y = (x-1)/(x+1)
    return 2*(y + y^3/3 + y^5/5)
end