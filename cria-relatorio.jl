include("relatorio.jl")

function como_usar()
  println("julia cria-relatorio.jl DADOS.csv ESTADO")
  println("")
  println("  Cria relatório sobre o estado ESTADO a partir dos dados em")
  println("  DADOS.csv.")
end

if length(ARGS) < 2
  como_usar()
  exit(1)
end

dados, cabecalho = readcsv(ARGS[1], header=true)
relatorio_populacional_estado(dados, ARGS[2])
